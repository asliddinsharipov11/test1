import { createApp } from 'vue'
import App from './App.vue'
import router from '@/plugins/router'
import components from '@/components/UI'
import store from './plugins/js/store'
import './assets/tailwind.css'

const app =createApp(App)

components.forEach(component => {
    app.component(component.name,component)
});

app.use(router)
app.use(store)
app.mount('#app')