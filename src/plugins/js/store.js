import {createStore} from "vuex"


const store = createStore({
    modules: {
        
       
    },
    state: {
        backUrl: 'http://aivamarket.uz',
        media: null
    },
    mutations: {
      updatemedia(state, data) {
          state.media = data
      }
    },
    getters: {
        getUrl(state) {
            return state.backUrl
        }
    }
})
export default store
