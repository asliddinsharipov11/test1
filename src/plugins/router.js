import {createWebHistory, createRouter}  from "vue-router";



const router = createRouter({
    history: createWebHistory(),
    routes: [
      // home start
      {
        path: '/',
        component:()=> import('../pages/home/Home'),
      },
      // home end
      // form start
      {
        path: '/form',
        component:()=> import('../pages/form/Form'),
      },
      // form end
       // form start
      {
        path: '/students',
        component:()=> import('../pages/student/Students'),
      },
      // form end
      // form start
      {
        path: '/login',
        component:()=> import('../pages/login/Login'),
      },
      // form end
      //form start
      {
        path: '/signup',
        component:()=> import('../pages/signup/Singup'),
      },
      // form end
    
      
    ],
   
  });

export default router;