import MyNavbar from '@/components/UI/MyNavbar'
import BarModal from '@/components/UI/BarModal'
import MainModal from '@/components/UI/MainModal'
import DeleteModal from '@/components/UI/DeleteModal'
import ImgModal from '@/components/UI/ImgModal'

export default [
    MyNavbar,
    BarModal,
    MainModal,
    DeleteModal,
    ImgModal
]